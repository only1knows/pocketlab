import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {
  Card,
  Paragraph,
  Headline,
  Caption,
  ActivityIndicator,
  BottomNavigation,
} from 'react-native-paper';
import Markdown from 'react-native-markdown-renderer';
import GitLabAPI from '../../../GitLabAPI';
import Description from './Description';
import Discussion from '@components/Discussion';
import {withTitle} from '@components/Title';
import moment from 'moment';

const styles = StyleSheet.create({
  codeInline: {
    backgroundColor: '#fbe5e1',
    color: '#c0341d',
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 20,
    paddingRight: 2,
  },
});

const routes = [
  {key: 'description', title: 'Issue', icon: 'alert-circle-outline'},
  {key: 'discussion', title: 'Discussion', icon: 'forum'},
];

const Issue = props => {
  const {params} = props.match;
  const [issue, setIssue] = useState(null);
  const [notes, setNotes] = useState([]);
  const [index, setIndex] = useState(0);

  const DescRoute = () => <Description issue={issue} />;
  const DiscussionRoute = () => <Discussion notes={notes} />;
  const renderScene = BottomNavigation.SceneMap({
    description: DescRoute,
    discussion: DiscussionRoute,
  });

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.issue(params.project, params.issueId).then(response => {
      props.setTitle(`Issue #${response.iid}`);
      setIssue(response);
    });
    gitlab.issueNotes(params.project, params.issueId, 'asc').then(response => {
      setNotes(response);
    });
  }, [params.issueId, params.project, props]);

  if (issue === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }
  return (
    <BottomNavigation
      navigationState={{index, routes}}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
};

export default withTitle(Issue);
